package com.gsb.demor2dbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.gsb.demor2dbc")
public class DemoR2dbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoR2dbcApplication.class, args);
    }

}
