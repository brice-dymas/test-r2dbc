package com.gsb.demor2dbc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 * Projet :  demo-r2dbc
 * Package :  com.gsb.demor2dbc.domain
 * File :  Player
 * Created on : 2021, Wednesday 24 of November
 * Created at : 5:23 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Player {
    @Id
    Long id;
    String name;
    Integer age;
}
