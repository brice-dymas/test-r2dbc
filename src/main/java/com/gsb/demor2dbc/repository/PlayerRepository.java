package com.gsb.demor2dbc.repository;

import com.gsb.demor2dbc.domain.Player;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * Projet :  demo-r2dbc
 * Package :  com.gsb.demor2dbc.repository
 * File :  PlayerRepository
 * Created on : 2021, Wednesday 24 of November
 * Created at : 5:32 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface PlayerRepository extends ReactiveCrudRepository<Player, Long> {
    Flux<Player> findAllByName(String name);

    Flux<Player> findAllByAge(int age);
}
